; 22/06/2018 - Tech'Manager Client Installer;

#define MyAppId "{F9A6451D-C3FB-4052-A34E-C3EEF4699699}"
#define MyAppName "Tech'Manager"
#define MyAppVersion "19.0.0.1"
#define MyAppPublisher "Algo'Tech Informatique"
#define MyAppURL "https://www.algotech-informatique.com/"
#define MyAppExeName "TechManager.exe"

[Setup]
AppId = {{#MyAppId}
AppName = {#MyAppName}
AppVersion = {#MyAppVersion} 
AppVerName = {#MyAppName} {#MyAppVersion}
AppCopyright = "(C) Algo'Tech Informatique 2018"
VersionInfoVersion     = {#MyAppVersion}
VersionInfoTextVersion = {#MyAppVersion}
OutputBaseFilename = "Installateur Tech'Manager"
DefaultDirName = "{pf}\Algotech\TechManager"
OutputDir = "01_Compil_Install_TechManager"
DefaultGroupName = Algotech\Tech'Manager
WizardImageFile = "00_Pdt_Install_TechManager\Image\Algotech_164x314.bmp"
UninstallDisplayName = "Tech'Manager"
VersionInfoCompany = "Algo'Tech Informatique"
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
LanguageDetectionMethod=locale
AlwaysUsePersonalGroup=false
Compression=lzma
SolidCompression=true
DisableWelcomePage=no
LicenseFile="00_Pdt_Install_TechManager\licence.txt"
;InfoBeforeFile="00_Pdt_Install_TechManager\info.txt"
UserInfoPage=no

[Components]
Name: Client; Description: "Tech'Manager Client"; Types: full compact custom
name: ClientLite; Description: "Tech'Manager Client Lite"; Types: full compact custom
Name: Pdf2TextPilot; Description: "PDF2Text Pilot"; Types: full;
Name: GhostScript; Description: "GhostScript Pilot"; Types: full;

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: fr; MessagesFile: compiler:Languages\French.isl

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}";
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 0,6.1

[Icons]
Name: "{commondesktop}\Tech'Manager"; Filename: "{app}\TechManager.exe"; IconFilename: "{app}\Image\TechManager.ico"; Components: Client;
Name: "{group}\Tech'Manager"; Filename: "{app}\TechManager.exe"; IconFilename: "{app}\Image\TechManager.ico"; Components: Client;
name: "{group}\Tech'Manager Support"; Filename: "{app} \Aide\TechManager_Aide.PDF";  IconFilename: "{app}\Image\Help.ico"; 
Name: "{group}\Desinstaller Tech'Manager"; Filename: "{uninstallexe}";

;Root: HKLM; Subkey: "Software\ALGOTECH\TechManager"; Flags: uninsdeletekeyifempty
;Root: HKLM; Subkey: "Software\ALGOTECH\TechManager\HCL_GED"; Flags: uninsdeletekey
;Root: HKLM; Subkey: "Software\ALGOTECH\TechManager\HCL_GED"; ValueType: string; ValueName: "PATH"; ValueData: "{app}\TechManagerLite.exe"
;Root: HKLM; Subkey: "Software\ALGOTECH\TechManager\INFOS"; Flags: uninsdeletekey
;Root: HKLM; Subkey: "Software\ALGOTECH\TechManager\INFOS"; ValueType: string; ValueName: "PATH"; ValueData: "{app}";
[Registry]
Root: HKU; Subkey :".DEFAULT\Software\ALGOTECH\TechManager"; Flags: uninsdeletekeyifempty
Root: HKU; Subkey :".DEFAULT\Software\ALGOTECH\TechManager\GED"; Flags: uninsdeletekey
Root: HKU; Subkey :".DEFAULT\Software\ALGOTECH\TechManager\GED"; ValueType: string; ValueName: "PATH"; ValueData: "{app}\TechManagerLite.exe"
Root: HKU; Subkey :".DEFAULT\Software\ALGOTECH\TechManager\INFOS"; Flags: uninsdeletekey
Root: HKU; Subkey :".DEFAULT\Software\ALGOTECH\TechManager\INFOS"; ValueType: string; ValueName: "PATH"; ValueData: "{app}\"
 
[Dirs]
Name: {app}; Permissions: users-full

[Files]
Source: "00_Pdt_Install_TechManager\TechManager\TechManager.exe"; DestDir: "{app}"; Components: Client; Permissions: users-full; 
Source: "00_Pdt_Install_TechManager\TechManager\TechManagerLite.exe"; DestDir: "{app}"; Components: ClientLite; Permissions: users-full;
Source: "00_Pdt_Install_TechManager\Outils\PDF2Text.msi"; DestDir: "{app}\Outils"; Components: Pdf2TextPilot; Permissions: users-full; Flags: deleteafterInstall; 
Source: "00_Pdt_Install_TechManager\Outils\gs925w32.exe"; DestDir: "{app}\Outils"; Components: GhostScript; Permissions: users-full; Flags: deleteafterInstall;

; INI
Source: "00_Pdt_Install_TechManager\Language\Table1.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table2.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table1-AR.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table2-AR.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table1-EN.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table2-EN.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table1-ES.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table2-ES.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table1-FR.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table2-FR.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table1-PO.INI"; DestDir: "{commonappdata}\AlgoTech";
Source: "00_Pdt_Install_TechManager\Language\Table2-PO.INI"; DestDir: "{commonappdata}\AlgoTech"; 
Source: "00_Pdt_Install_TechManager\Language\Table1.INI"; DestDir: "{commonappdata}\AlgoTech"; Permissions: users-full;
Source: "00_Pdt_Install_TechManager\Language\Table2.INI"; DestDir: "{commonappdata}\AlgoTech"; AfterInstall: DeleteOldTableIni; Permissions: users-full;

; Excel Service DWG
Source: "00_Pdt_Install_TechManager\Service\ModeleFichierExportTexteDxfDwgXLS.xls"; DestDir: "{app}\Service\"; Components: Client; Permissions: users-full; Flags:onlyifdoesntexist ignoreversion; Check: IsUpgrade;
Source: "00_Pdt_Install_TechManager\Service\ModeleFichierExportZoneDxfDwgXLS.xls"; DestDir: "{app}\Service\"; Components: Client; Permissions: users-full;  Flags:onlyifdoesntexist ignoreversion; Check: IsUpgrade;
Source: "00_Pdt_Install_TechManager\Service\Test_FichierExportTexteDxfDwgXLS.xls"; DestDir: "{app}\Service\"; Components: Client; Permissions: users-full;  Flags:onlyifdoesntexist ignoreversion; Check: IsUpgrade; 
Source: "00_Pdt_Install_TechManager\Service\Test_FichierExportZoneDxfDwgXLS.xls"; DestDir: "{app}\Service\"; Components: Client; Permissions: users-full;   Flags:onlyifdoesntexist ignoreversion; Check: IsUpgrade;

; DLL
Source: "00_Pdt_Install_TechManager\TechManager\sx32w.dll"; DestDir: "{app}"; Permissions: users-full; Flags:onlyifdoesntexist ignoreversion;    Check: not IsUpgrade;
Source: "00_Pdt_Install_TechManager\TechManager\midas.dll"; DestDir: "{app}"; Permissions: users-full; Flags:onlyifdoesntexist ignoreversion;    Check: not IsUpgrade;
Source: "00_Pdt_Install_TechManager\TechManager\qtintf70.dll"; DestDir: "{app}"; Permissions: users-full; Flags:onlyifdoesntexist ignoreversion; Check: not IsUpgrade;
;Source: "00_Pdt_Install_TechManager\TechManager\XXINI.BIN"; DestDir: "{app}";  Flags:onlyifdoesntexist ignoreversion;
Source: "\\serveurbidart\Installation-Produit\40_installation\02_INSTALL_SHIELD\_install_UAC_2018\XXINI.BIN"; DestDir: "{app}";  Permissions: users-full; Flags:onlyifdoesntexist ignoreversion;
Source: "00_Pdt_Install_TechManager\TechManager\dbexpida40.dll"; DestDir: "{app}"; Permissions: users-full; Flags:onlyifdoesntexist ignoreversion; Check: not IsUpgrade; 
Source: "00_Pdt_Install_TechManager\TechManager\dbxfb.dll"; DestDir: "{sys}"; Permissions: users-full;      Flags:onlyifdoesntexist; Check: not IsUpgrade;

; Excel
;Source: "00_Pdt_Install_TechManager\Schema\modele distribution et synoptique.xls"; DestDir: "{app}\Schema\"; Components: Client; Permissions: users-full;
; Image
Source: "00_Pdt_Install_TechManager\image\*"; DestDir: "{app}\image"; Flags: ignoreversion recursesubdirs
; Aide
Source: "00_Pdt_Install_TechManager\Aide\TechManager_Aide.PDF"; DestDir: "{app}\Aide"; Components: Client;
;Service DWG
Source: "00_Pdt_Install_TechManager\Service\*"; DestDir: "{app}\Service"; Flags: ignoreversion recursesubdirs 
;GhostScript
;Source: "00_Pdt_Install_TechManager\Outils\gs925w32.exe"; DestDir: "{app}\Outils\"; AfterInstall: RunOtherInstaller

[Run]
Filename: msiexec.exe; parameters: "/i ""{app}\Outils\PDF2Text.msi"; Components: Pdf2TextPilot;
Filename: "{app}\Outils\gs925w32.exe"; Components: GhostScript;
Filename: {app}\{cm:AppName}.exe; Description: {cm:LaunchProgram,{cm:AppName}}; Flags: nowait postinstall skipifsilent;

[INI]
Filename: "{app}\TechManager_ctrl.ini"; Section: "CONNECTION"; Key: "IP"; String:   "{code:GetConfigurationInfo|BACKEND_IP}"        ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "CONNECTION"; Key: "PORT"; String: "{code:GetConfigurationInfo|BACKEND_PORT}"      ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "LICENCE"; Key: "IP"; String:   "{code:GetConfigurationInfo|LICENCE_IP}"           ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "LICENCE"; Key: "PORT"; String: "{code:GetConfigurationInfo|LICENCE_PORT}"         ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "CONTROL_OPTIONS"; Key: "FILE_PDF2TEXT"; String: "{app}\Outils\PDF2Text.exe"       ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "CONTROL_OPTIONS"; Key: "HISTO_LAUNCH"; String: "0"                                ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "CONTROL_OPTIONS"; Key: "FILE_FONDECRAN"; String: "{app}\image\TechManager_bg.bmp" ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "CONTROL_OPTIONS"; Key: "PDF_READER"; String: "{code:GetConfigurationInfo|PDF_EXE}";Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "CONTROL_OPTIONS"; Key: "LANGUE"; String:  {cm:Langue}                             ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "CONTROL_OPTIONS"; Key: "PDM_DEFAULT_TEXT"; String:  "DESCRIPTION"                 ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "CONTROL_OPTIONS"; Key: "PDM_DEFAULT_COLOR"; String:  "$000000"                    ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "SEARCH"; Key: "HUPI_SEARCH"; String: "1"                                          ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "SEARCH"; Key: "MAX_RESULT"; String: "500"                                         ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "SEARCH"; Key: "NUMBER_DAYS"; String: "30"                                         ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "Libelle"; Key: "TYPESCH"; String: "Schema"                                        ;Flags: createkeyifdoesntexist;
Filename: "{app}\TechManager_ctrl.ini"; Section: "Libelle"; Key: "TYPESYN"; String: "Synoptique"                                    ;Flags: createkeyifdoesntexist;

[UninstallRun]
;Filename: {app}\VKServiceUninstall.bat; Flags: hidewizard runhidden; Components: Serveur;

[InstallDelete]
Type:  filesandordirs; Name: {app}; Check: Not IsUpgrade;
 
[UninstallDelete]
Type: filesandordirs; Name: {app};
Type: files; Name: "{app}\Manquant.txt"; Components: Client;
Type: files; Name: "{app}\bugreport.txt";

[CustomMessages]
en.NewerVersionExists=A newer version of {#MyAppName} is already installed.%n%nInstaller version: {#MyAppVersion}%nCurrent version: 
fr.NewerVersionExists=Une nouvelle version de l'application {#MyAppName} a �t� d�j� install�e.%n%nVersion installation: {#MyAppVersion}%nVersion Actuelle: 

en.MyAppOld=The Setup detected application version 
en.MyAppRequired=The installation of {#MyAppName} requires MyApp to be installed.%nInstall MyApp before installing this update.%n%n
en.MyAppTerminated=The setup of update will be terminated.

fr.MyAppOld=L'installateur a trouv� une version de l'application
fr.MyAppRequired=L'installateur de {#MyAppName} demande l'instalatirequires MyApp to be installed.%nInstall MyApp before installing this update.%n%n
fr.MyAppTerminated=The setup of update will be terminated.

en.serverlicence=License server info
en.Addresselicence=What is the License server IP?
en.infoserverlicence=Enter the license server information and then click continue.

fr.serverlicence=Information du serveur de licences
fr.Addresselicence=Quelle est l'addresse du serveur de licence?
fr.infoserverlicence=Indiquez les informations du serveur de licences et apr�s cliquez sur continuer.

en.serverlicenceIP=Server IP:
en.serverlicencePort=Server Port:
en.serverbackendIP=Server Backend IP:
en.ServerbackendPort=Server Backend Port:

fr.serverlicenceIP=Serveur IP:
fr.serverlicencePort=Serveur Port:
fr.serverbackendIP=Serveur Backend IP:
fr.ServerbackendPort=Serveur Backend Port:

en.ServerBackend=Backend server info
en.ServerBackendInfo=What is the Backend server IP?
en.ServerBackendLong=Enter the Backend server information and then click continue.

fr.ServerBackend=Information du serveur Backend
fr.ServerBackendInfo=Quelle est l''addresse du serveur de Backend?
fr.ServerBackendLong=Indiquez les informations du serveur Backend et apr�s cliquez sur continuer.

en.confirmServeurLicence=You must add the License server address.
en.confirmlicencePort=You must add the License server Port.
en.ConfirmServeurBackend=You must add the Backend server address.
en.ConfirmBackendPort=You must add the Backend server Port.

fr.confirmServeurLicence=Vous devez ajouter l'addresse du serveur de licences.
fr.confirmlicencePort=Vous devez ajouter le Port du serveur de licences.
fr.ConfirmServeurBackend=Vous devez ajouter l''addresse du serveur Backend.
fr.ConfirmBackendPort=Vous devez ajouter le Port du serveur Backend.

en.oldversion=An old version of app was detected. Do you want to uninstall it?
fr.oldversion=Une version ancienne de l'application a �t� trouv�e. Voulez-vous la d�sinstaller?

en.AppName=TechManager
en.LaunchProgram=Start Tech'Manager after finishing installation

fr.AppName=TechManager
fr.LaunchProgram=D�marrer Tech'Manager apr�s l'installation

en.Langue=EN
fr.Langue=FR
                                                                   
en.PDFPathTitle=Select PDF Executable location                                                                   
fr.PDFPathTitle=S�lection de l'application de lecture des PDF

en.PDFPathTitle1=Where is PDF Executable installed ?
en.PDFPathTitle2=Select where PDF Executable is located, 
en.PDFPathTitle3= then click Next.
fr.PDFPathTitle1=O� se trouve l'application pour la lecture des PDF?
fr.PDFPathTitle2=Pr�cisez o� se trouve l'application de lecture des DPF,
fr.PDFPathTitle3= et apr�s, cliquez sur suivant.

en.TitleMAJ=Old Version detected
fr.TitleMAJ=Une version ancienne a �t� trouv�e
en.DescMAJ=An old version of app was detected. Do you want to update it?
fr.DescMAJ=Une version ancienne de l'application a �t� trouv�e. Voulez-vous la mettre � jour?
en.MessageMAJ=Options
fr.MessageMAJ=Options
en.OptMAJ=Upgrade/Update
fr.OptMAJ=Mise � Jour
en.OptInstall=Install
fr.OptInstall=Installation
                                                                     
[Code]
var
  dirDestBD : string;
  MainInstallUpdatePage : TWizardPage;
  UserLicenceServerPage : TInputQueryWizardPage;
  UserBackendServerPage : TInputQueryWizardPage;
  ProgressPage  : TOutputProgressWizardPage;
  ProgressPage1 : TOutputProgressWizardPage;
  
  FilePage : TInputFileWizardPage;
  ProgressPage2 : TOutputProgressWizardPage;

  rdbInstall : TNewRadioButton;
  rdbUpdate :  TNewRadioButton;
  bUpgrade : Boolean; 
  bExists : Boolean;
    
Function IsUpgrade : Boolean;
begin
  result := bUpgrade;
End;

function ConvertFileToUTF8(FileName: string): Boolean;
var
  Lines: TArrayOfString;
begin
  Result :=
    LoadStringsFromFile(FileName, Lines) and
    SaveStringsToUTF8File(FileName, Lines, False);
end;

Function GetUninstallString(b32 : boolean): String;
var
  sUnInstPath: String;  
  sUnInstallString: String;
begin
  // sUnInstPath := ExpandConstant('SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\#MyAppId_is1'); // for ver 1.4.5 and newer
  if b32 Then
    sUnInstPath := 'Software\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppId}_is1'
  Else
    sUnInstPath := 'SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppId}_is1';    
  sUnInstallString := '';
  if not RegQueryStringValue(HKLM, sUnInstPath, 'UninstallString', sUnInstallString) then
    RegQueryStringValue(HKCU, sUnInstPath, 'UninstallString', sUnInstallString);
  Result := sUnInstallString;
  // MsgBox(sUnInstallString, mbInformation, MB_OK);
end;
 
function InitializeSetup(): boolean;
var
  sReg32 : string;
  sReg64 : string;
  b32 : boolean;
begin

  bExists := False;
  Result := True; // in case when no previous version is found 
  sReg32 := 'Software\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppId}_is1';
  sReg64 := 'SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{#MyAppId}_is1';
  if RegValueExists(HKEY_LOCAL_MACHINE, sReg32, 'UninstallString') then  // Your App GUID/ID 
    Begin
      b32 := true;
      bExists := True;
    End
  Else
    Begin
     IF RegValueExists(HKEY_LOCAL_MACHINE,sReg64, 'UninstallString') Then
       begin
         b32 := False;
         bExists := True;
       End;
    End;
end;

procedure InitializeWizard();
// Ajoute une page d'avertissements.
var
  lblBlobFileFolder : TLabel;
begin

  IF bExists Then
    Begin          
  
      MainInstallUpdatePage := CreateCustomPage(wpWelcome, customMessage('TitleMAJ'), customMessage('DescMAJ'));
            
      lblBlobFileFolder := TLabel.Create(MainInstallUpdatePage);
      lblBlobFileFolder.parent := MainInstallUpdatePage.Surface;
      lblBlobFileFolder.Top := 0;
      lblBlobFileFolder.Left := 0;
      lblBlobFileFolder.Caption := customMessage('MessageMAJ');

      rdbUpdate :=  TNewRadioButton.Create(MainInstallUpdatePage);
      rdbUpdate.Parent := MainInstallUpdatePage.Surface;
      rdbUpdate.Top := 40;
      rdbUpdate.Left := 0;
      rdbUpdate.Caption := customMessage('OptMAJ');
      rdbUpdate.Checked := True;

      rdbInstall := TNewRadioButton.Create(MainInstallUpdatePage);
      rdbInstall.Parent := MainInstallUpdatePage.Surface;
      rdbInstall.Top := 80;
      rdbInstall.Left := 0;
      rdbInstall.Caption := customMessage('OptInstall');         
    End;

  UserLicenceServerPage := CreateInputQueryPage(wpLicense,
    customMessage('serverlicence'), customMessage('Addresselicence'),
    customMessage('infoserverlicence'));
  UserLicenceServerPage.Add(customMessage('serverlicenceIP'), False);
  UserLicenceServerPage.Add(customMessage('serverlicencePort'), False);

  ProgressPage := CreateOutputProgressPage( customMessage('serverlicence'), customMessage('Addresselicence'));

  UserBackendServerPage := CreateInputQueryPage(UserLicenceServerPage.ID,
    customMessage('ServerBackend'), customMessage('ServerBackendInfo'),
    customMessage('ServerBackendLong'));
  UserBackendServerPage.Add(customMessage('serverbackendIP'), False);
  UserBackendServerPage.Add(customMessage('ServerbackendPort'), False);

  ProgressPage1 := CreateOutputProgressPage( customMessage('ServerBackend'), customMessage('ServerBackendInfo'));

  FilePage := CreateInputFilePage(wpSelectDir, customMessage('PDFPathTitle'), customMessage('PDFPathTitle1'), customMessage('PDFPathTitle2') + customMessage('PDFPathTitle3'));
  FilePage.Add(customMessage('PDFPathTitle'), 'PDF EXE|*.exe', '.exe');

  FilePage.Edits[0].ReadOnly := True;
  FilePage.Edits[0].Color := clBtnFace;
 
  ProgressPage2 := CreateOutputProgressPage(customMessage('PDFPathTitle'), customMessage('PDFPathTitle1'));
end;

Function NextButtonClick(CurPageID: Integer): Boolean;
begin
  { Validate certain pages before allowing the user to proceed }
  Result := true;  
  if bExists Then
    Begin
      IF (CurPageID = MainInstallUpdatePage.ID) Then
        Begin
          bUpgrade := rdbUpdate.Checked;      
        End;
    End;

  if CurPageID = UserLicenceServerPage.ID then 
    begin
      if UserLicenceServerPage.Values[0] = '' then 
        begin
          MsgBox(customMessage('confirmServeurLicence'), mbError, MB_OK);
          Result := False;
        end
      Else
        Begin
          If UserLicenceServerPage.Values[1] = '' then 
            begin
              MsgBox(customMessage('confirmlicencePort'), mbError, MB_OK);
              Result := False;
            end;  
        End;  
    end;
  if CurPageID = UserBackendServerPage.ID then 
    begin
      if UserBackendServerPage.Values[0] = '' then 
        begin
          MsgBox(customMessage('ConfirmServeurBackend'), mbError, MB_OK);
          Result := False;
        end
      Else
        Begin
          If UserBackendServerPage.Values[1] = '' then 
            begin
              MsgBox(customMessage('ConfirmBackendPort'), mbError, MB_OK);
              Result := False;
            end;    
        End;
    end; 
end;

Function ShouldSkipPage(PageId : Integer) : Boolean;
Begin
  If (pageId = UserLicenceServerPage.ID) Then
    result := (bUpgrade);
  If (pageId = UserBackendServerPage.ID) Then
    result := (bUpgrade);
  If (pageId = FilePage.ID) Then
    result := (bUpgrade);
End;

procedure CurStepChanged(CurStep: TSetupStep);
var
  sIni : string;
begin
  if CurStep = ssPostInstall then begin
    sIni := '{app}\TechManager_ctrl.ini'; 
    ConvertFileToUTF8(sIni);  
  End;
End;

function GetNomMachine( chemin: string): string;
var
  i, iMax : integer;
  s : string;
begin
  result := '';
  s := ExpandUNCFileName( chemin);
  if Length( s) > 2 then
    begin
      if (Length( s) > 1) and (s[1] = '\') and (s[2] = '\') then
        begin
          i := 3;
          iMax := Length( s) + 1;
          while i < iMax do
            begin
              if s[i] = '\' then
                i := iMax
              else
                begin
                  result := result + s[i];
                  i := i + 1;
                end;
            end;
        end;
    end;
  if result = '' then
    result := GetComputerNameString;
end;

function UpdateReadyMemo( Space, NewLine, MemoUserInfoInfo, MemoDirInfo, MemoTypeInfo,
  MemoComponentsInfo, MemoGroupInfo, MemoTasksInfo: String): String;
var
  S: String;
begin
  S := S + MemoDirInfo + NewLine + NewLine;
  if MemoTypeInfo <> '' then
    S := S + MemoTypeInfo + NewLine + NewLine;
  if MemoComponentsInfo <> '' then
    S := S + MemoComponentsInfo + NewLine + NewLine;
  if MemoGroupInfo <> '' then
    S := S + MemoGroupInfo + NewLine + NewLine;
  if MemoTasksInfo <> '' then
    S := S + MemoTasksInfo + NewLine + NewLine;
  if MemoUserInfoInfo <> '' then
    S := S + MemoUserInfoInfo + NewLine + NewLine;
  Result := S;
end;

function GetDirDestBD( Param: String): string;
begin
  Result := dirDestBD;
end;

function GetVersionFic( LeFic: String): string;
begin
  GetVersionNumbersString( LeFic, result);
end;

Function GetConfigurationInfo(Param : String) : String;
Begin
  IF Param = 'LICENCE_IP' Then
    result := UserLicenceServerPage.Values[0];
  IF Param = 'LICENCE_PORT' Then
    result := UserLicenceServerPage.Values[1];
  IF Param = 'BACKEND_IP' Then
    result := UserBackendServerPage.values[0];
  IF Param = 'BACKEND_PORT' Then
    result := UserBackendServerPage.values[1];  
  IF Param = 'PDF_EXE' Then
    result := FilePage.Values[0];
End;

procedure DeleteOldTableIni();
var
  fic : string;
begin
  fic := AddBackslash( ExpandConstant( '{app}')) +'Table2.ini';
  if FileExists( fic) then
    begin
      try
        DeleteFile( fic);
      except 
        fic := fic;
      end;
    end; 

  fic := AddBackslash( ExpandConstant( '{app}')) +'Table1.ini';
  if FileExists( fic) then
    begin
      try
        DeleteFile( fic);
      except 
        fic := fic;
      end;
    end; 
end;

procedure RunOtherInstaller;
var
  ResultCode: Integer;
begin
  if not Exec(ExpandConstant('{app}\Outils\gs925w32.exe'), '', '', SW_SHOWNORMAL, ewWaitUntilTerminated, ResultCode) then
    MsgBox('Other installer failed to run!' + #13#10 + SysErrorMessage(ResultCode), mbError, MB_OK);
end;
